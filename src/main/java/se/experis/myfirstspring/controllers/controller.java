package se.experis.myfirstspring.controllers;

import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class controller {

    // take the query and return it with a message
    @RequestMapping(value = "/getname", method = GET)
    public String hello(@RequestParam("name") String name) {
        return "Hello " + name;
    }


    // Take the query and reverse it.
    @RequestMapping(value = "/reversetext", method = GET)
    public String reverseArray(@RequestParam("text") String text) {

        // stringbuilder has a nice solution for reversing strings
        StringBuilder reverseString = new StringBuilder(text);
        String stringReverse = reverseString.reverse().toString();
        boolean isPalindrome = text.equals(stringReverse);

        String returnText = text + " reversed is " + stringReverse;

        if (isPalindrome) {
            return returnText.concat(", it's a palindrome! yay.");
        } else {
            return returnText.concat(", it's not a paindrome.");
        }
    }


}
